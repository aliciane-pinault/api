---
marp: true
theme: uncover
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---
<style scoped>
h1 {
  font-size: 90px;
}
p {
    font-size: 30px;
    text-align: center;
    columns: 2;
}
</style>

# Application </br> Programming </br> Interface

Roncajolo Gerald
T/ 06 71 50 61 53
grc@necol.org
https://gitlab.com/training788/api
Nov. 2022

---

# Disclaimer

These slides are for training or educational purposes.
They do not replace reference documentation.

This is this document's first version, November 2022.
If you read this in 2022+, check for depreciation.

---

# Application Programming Interface

- Define communication interactions between different softwares, or components of a software

---

# Why use APIs ?

1. Enable mobile as an additional channel
2. Grow ecosystems: customer (B2C) or partner ecosystems (B2B)
3. Develop massive reach for transaction or content distribution
4. Power new business models
5. Generate internal innovation

--- 

# Design pattern

![bg right:30% 80%](assets/interface-implementation.png)

- Separating interface from implementation

- Façade design pattern
  - A simplified interface to a larger body of code
  - Make software easier and convenient to use
  - Reduce dependencies
  - Wrap a poorly designed APIs with a single well-designed API

---

- Modern applications often use a **Presentation**(UI)-**Domain**(aka business logic)-**Data**(data access) Layreing approach.  
  - Separation of concerns
  - Enables modularisation, and in turn **maintainability**

<!---
So you often see web applications divided into a web layer that knows about handling HTTP requests and rendering HTML, a business logic layer that contains validations and calculations, and a data access layer that sorts out how to manage persistent data in a database or remote services.
--->